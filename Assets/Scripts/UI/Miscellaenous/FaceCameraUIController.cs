using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCameraUIController : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Camera _camera;

    private void Update()
    {
        transform.LookAt(transform.position + _camera.transform.rotation * Vector3.forward, _camera.transform.rotation * Vector3.up);
    }
}
