using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class ButtonUIAnimation : MonoBehaviour
{
    [Header("Configuration")]
    [SerializeField] private bool _overrideClick;
    [SerializeField] private Vector3 _targetScale;

    private Button button;
    private Vector3 originalScale;

    private Sequence tweenSequence;

    private UnityEvent evtOverrideClick = new UnityEvent();

    protected void Awake()
    {
        button = GetComponent<Button>();

        if (_overrideClick)
        {
            evtOverrideClick = button.onClick;
            button.onClick = new Button.ButtonClickedEvent();
        }

        button.onClick.AddListener(Play);

        originalScale = transform.localScale;
    }

    public void Play()
    {
        if (tweenSequence != null) return;

        tweenSequence = DOTween.Sequence();
        tweenSequence.SetUpdate(true);
        tweenSequence.Insert(0, transform.DOScale(_targetScale, 0.1f));
        tweenSequence.Insert(0.1f, transform.DOScale(Vector3.one, 0.1f));

        tweenSequence.OnComplete(() => OnComplete());
        tweenSequence.Play();
    }

    private void OnComplete()
    {
        tweenSequence = null;

        if (_overrideClick)
        {
            evtOverrideClick.Invoke();
        }

        ResetProperties();
    }

    public void Stop()
    {
        tweenSequence?.Kill();
        tweenSequence = null;

        ResetProperties();
    }

    private void ResetProperties()
    {
        transform.localScale = originalScale;
    }
}
