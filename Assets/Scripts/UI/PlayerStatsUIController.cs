﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerStatsUIController : MonoBehaviour
{
    [Header("References")]
    [SerializeField] private Text _scoreText = null;
    [SerializeField] private Text _fundsText = null;
    [SerializeField] private Text _coreHealthText = null;
    [SerializeField] private Text _waveText = null;
    [SerializeField] private Text _waveTimerText = null;

    private StageMode stageMode;

    private void Start()
    {
        stageMode = GameMode.GetActive<StageMode>();
    }

    private void Update()
    {
        _scoreText.text = stageMode.PlayerCore.PlayerState.Score.ToString("N0");
        _fundsText.text = stageMode.PlayerCore.PlayerState.Funds.ToString("N0");
        _coreHealthText.text = stageMode.PlayerCore.Health.CurrentHp.ToString("N0");
        _waveText.text = stageMode.CurrentWave.ToString("N0");

        System.TimeSpan time = System.TimeSpan.FromSeconds(stageMode.TimeUntilNextWave);
        _waveTimerText.text = time.ToString(@"m\:ss");
    }

    public void ExitGame()
    {
        stageMode.LoadMainMenu();
    }
}
