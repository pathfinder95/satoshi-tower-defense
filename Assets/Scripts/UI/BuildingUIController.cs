using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingUIController : MonoBehaviour
{
    [System.Serializable]
    public class BuildingButton
    {
        public Button Button;
        public Text NameText;
        public Text CostText;
        public Image IconImage;

        public BaseBuilding AssignedBuilding { get; private set; }

        public void Setup(BaseBuilding building)
        {
            AssignedBuilding = building;

            Button.name = "PurchaseButton: " + building.Id;
            NameText.text = AssignedBuilding.Name;
            CostText.text = "$" + AssignedBuilding.BuildCost.ToString("N0");
            IconImage.overrideSprite = AssignedBuilding.UIIcon;
        }
    }

    [Header("Configuration")]
    [SerializeField] private Vector2 _anchorOffset;

    [Header("References")]
    [SerializeField] private BuildingButton[] _buildingButtons;
    [SerializeField] private FadeUIAnimation _buildingPanel;
    [SerializeField] private FadeUIAnimation _purchasePanel;
    [SerializeField] private FadeUIAnimation _sellPanel;

    public BaseBuildingPivot AssignedPivot { get; private set; }

    private Dictionary<GameObject, BuildingButton> buttonDictionary = new();
    private BuildingManager buildingManager;

    private void Start()
    {
        buildingManager = SingletonManager.Get<BuildingManager>();
        buildingManager.EvtShow.AddListener(OnShow);
        buildingManager.EvtHide.AddListener(OnHide);

        for (int i = 0; i < _buildingButtons.Length; i++)
        {
            _buildingButtons[i].Setup(buildingManager.BuildableTurrets[i].GetComponent<BaseBuilding>());
            buttonDictionary.Add(_buildingButtons[i].Button.gameObject, _buildingButtons[i]);
        }
    }

    private void Update()
    {
        if (!AssignedPivot) return;

        if (AssignedPivot.CurrentBuilding) _sellPanel.Show();
        else if (!AssignedPivot.CurrentBuilding) _purchasePanel.Show();

        for (int i = 0; i < _buildingButtons.Length; i++)
        {
            BuildingButton button = _buildingButtons[i];

            button.Button.interactable = buildingManager.CanPurchase(button.AssignedBuilding);

            Color color = (button.Button.interactable) ? Color.black : Color.red;
            color.a = button.CostText.color.a;
            button.CostText.color = color;
        }
    }

    public void OnShow(BaseBuildingPivot pivot)
    {
        AssignedPivot = pivot;

        // Convert pivot position to canvas anchored position
        Vector2 canvasPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, AssignedPivot.transform.position);
        Vector2 anchoredPosition = transform.InverseTransformPoint(canvasPoint);
        ((RectTransform)_buildingPanel.transform).anchoredPosition = anchoredPosition + _anchorOffset;

        _buildingPanel.Show();
    }

    public void OnHide()
    {
        _buildingPanel.Hide();
        _purchasePanel.Hide();
        _sellPanel.Hide();

        AssignedPivot = null;
    }

    public void Purchase(GameObject bindingObject)
    {
        buildingManager.Purchase(AssignedPivot, buttonDictionary[bindingObject].AssignedBuilding);
    }

    public void Sell()
    {
        buildingManager.Sell(AssignedPivot);
    }
}
