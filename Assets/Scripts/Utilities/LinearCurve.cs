﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

[System.Serializable]
public struct LinearCurve
{
    [SerializeField] 
    private float _base;

    [SerializeField] 
    private float _growth;

    public static float GetLinearValue(float baseValue, float growth, int time)
    {
        if (time <= 0) return 0;
        return baseValue + (growth * (time - 1));
    }

    public static float ArithmeticSeries(float baseValue, float growth, int startTime, int steps)
    {
        Assert.IsTrue(startTime >= 0);
        Assert.IsTrue(steps >= 0);
        if (startTime == 0) return 0;

        float startTerm = GetLinearValue(baseValue, growth, startTime);
        if (steps == 0) return startTerm;

        float endTerm = GetLinearValue(baseValue, growth, startTime + steps - 1);

        return steps * (startTerm + endTerm) / 2.0f;
    }

    public float GetValue(int time)
    {
        return GetLinearValue(_base, _growth, time);
    }

    public float GetCeilValue(int time)
    {
        float val = GetValue(time);
        return Mathf.Ceil(val);
    }

    public float GetFloorValue(int time)
    {
        float val = GetValue(time);
        return Mathf.Floor(val);
    }

    public int GetIntValue(int time)
    {
        return (int)GetValue(time);
    }

    public float GetRoundedValue(int time)
    {
        float val = GetValue(time);
        return Mathf.Round(val);
    }

    /// <summary>
    /// Compute for the sum of the values in the curve
    /// </summary>
    /// <param name="startTime"></param>
    /// <param name="steps"></param>
    /// <returns></returns>
    public float GetSeries(int startTime, int steps)
    {
        return ArithmeticSeries(_base, _growth, startTime, steps);
    }
}
