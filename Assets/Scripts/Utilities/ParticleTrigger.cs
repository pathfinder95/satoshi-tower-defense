using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleTrigger : MonoBehaviour
{
    [SerializeField] private float _overridePoolDuration = 0.0f;
    [SerializeField] private ParticleSystem _particleSystem;

    public ParticleSystem.MainModule MainModule => _particleSystem.main;

    public float Duration
    {
        get => MainModule.duration;
        set
        {
            ParticleSystem.MainModule main = MainModule;
            main.duration = value;
        }
    }

    private CountdownTimer cdTimer = null;

    public void Play(bool pool = false)
    {
        _particleSystem.Play(true);

        if (pool) Pool(Duration);
    }

    public void Play(bool pool, float delay)
    {
        _particleSystem.Play(true);

        if (pool) Pool(delay);
    }

    public void Stop()
    {
        _particleSystem.Stop(true);
    }

    public void Clear()
    {
        _particleSystem.Clear(true);
    }

    public void Pool(float delay)
    {
        if (cdTimer == null)
        {
            cdTimer = new();
            cdTimer.EvtReady.AddListener(OnReady);
        }

        cdTimer.DurationBase = (_overridePoolDuration > 0.0f) ? _overridePoolDuration : delay;
        cdTimer.Start(this);
    }

    private void OnReady(CountdownTimer timer)
    {
        cdTimer.Stop();
        Stop();
        gameObject.Pool();
    }
}
