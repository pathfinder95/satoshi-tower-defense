using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardOnDeath : MonoBehaviour
{
    [Header("Configuration")]
    [SerializeField] private int _fundsReward = 10;
    [SerializeField] private int _scoreReward = 10;

    private void Awake()
    {
        GetComponent<Health>().EvtDied.AddListener(OnDied);
    }

    private void OnDied(Health health, bool combatDamage)
    {
        if (!combatDamage) return;

        PlayerState player = GameMode.GetActive<StageMode>().PlayerCore.PlayerState;
        player.AddFunds(_fundsReward);
        player.AddScore(_scoreReward);
    }
}
