using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsOnDeath : MonoBehaviour
{
    [Header("Effects")]
    [SerializeField] private GameObject _combatDeathVFX = null;
    [SerializeField] private GameObject _coreDeathVFX = null;

    private void Awake()
    {
        GetComponent<Health>().EvtDied.AddListener(OnDied);
    }

    private void OnDied(Health health, bool combatDamage)
    {
        GameObject vfxPrefab = (combatDamage) ? _combatDeathVFX : _coreDeathVFX;
        GameObject vfx = PrefabPool.Get(vfxPrefab);
        vfx.transform.position = GetComponent<Unit>().GetPosition();
        vfx.GetComponent<ParticleTrigger>().Play(true);
    }
}
