using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [Header("Configuration")]
    [SerializeField] private LinearCurve _moveSpeedCurve;
    [SerializeField] private LinearCurve _rotationSpeedCurve;

    public Vector3[] Path { get; set; }
    public int CurrentPathIndex { get; set; } = 0;
    public Vector3 CurrentPath => Path[CurrentPathIndex];
    public int Wave { get; set; } = 1;
    public float MoveSpeed =>_moveSpeedCurve.GetValue(Wave);
    public float RotationSpeed => _rotationSpeedCurve.GetValue(Wave);
    
    private Health health;

    private void Awake()
    {
        health = GetComponent<Health>();
    }

    private void Update()
    {
        if (Path == null || Path.Length <= 0) return;
        if (!health.IsAlive) return;
        if (CurrentPathIndex >= Path.Length) return;

        Vector3 lookAtTarget = CurrentPath;//new(CurrentPath.x, this.transform.position.y, CurrentPath.z);
        Vector3 direction = lookAtTarget - transform.position;

        if (direction != Vector3.zero)
        {
            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * RotationSpeed);
        }

        if (direction.magnitude < 0.5f)
        {
            CurrentPathIndex++;
        }

        this.transform.Translate(0.0f, 0.0f, MoveSpeed * Time.deltaTime);
    }
}
