using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Spawner : MonoBehaviour
{
    public UnityEvent<Unit> EvtSpawned { get; } = new();
    public UnityEvent EvtSpawningCompleted { get; } = new();

    [Header("Configuration")]
    // Used a primitive timer instead of CountdownTimer since spawning can be concurrent
    [SerializeField] private float _spawnInterval = 2.0f;
    [SerializeField] private string _teamId = null;

    [Header("References")]
    [SerializeField] private Board _board = null;

    public bool IsSpawning => spawnHandlers.Count > 0;

    private Dictionary<int, Coroutine> spawnHandlers = new();
    private List<Unit> spawned = new();

    public IReadOnlyList<Unit> Spawned => spawned;

    public void Spawn(GameObject prefab, int amount)
    {
        Coroutine handler = StartCoroutine(Spawning(spawnHandlers.Count, prefab, amount));
        spawnHandlers.Add(spawnHandlers.Count, handler);
    }

    public void StopSpawning()
    {
        foreach(Coroutine spawning in spawnHandlers.Values)
        {
            StopCoroutine(spawning);
        }

        spawnHandlers.Clear();
    }

    private IEnumerator Spawning(int id, GameObject prefab, int amount)
    {
        int spawnedAmount = 0;
        float timer = 0;

        while (spawnedAmount < amount)
        {
            SpawnUnit(prefab);
            spawnedAmount++;
            timer = 0;

            while (timer < _spawnInterval)
            {
                timer += Time.deltaTime;
                yield return null;
            }
        }

        yield return null;
        spawnHandlers.Remove(id);

        if (!IsSpawning) EvtSpawningCompleted.Invoke();
    }

    private void SpawnUnit(GameObject prefab)
    {
        Unit newUnit = PrefabPool.Get(prefab).GetComponent<Unit>();
        newUnit.transform.SetParent(this.transform);

        // Setup
        newUnit.Initialize();
        newUnit.Team.Id = _teamId;
        newUnit.Health.EvtDied.AddListener(OnUnitDied);

        // Spawn Point Check, use start of path as Spawn Point
        if (newUnit.Type == UnitType.Ground)
        {
            Vector3[] groundPath = _board.GetGroundPath();
            newUnit.Movement.Path = groundPath;
            newUnit.transform.position = groundPath[0];
        }
        else if (newUnit.Type == UnitType.Air)
        {
            Vector3[] airPath = _board.GetAirPath(); 
            newUnit.Movement.Path = airPath;
            newUnit.transform.position = airPath[0];
        }

        spawned.Add(newUnit);
        EvtSpawned.Invoke(newUnit);
    }

    private void OnUnitDied(Health health, bool combatDamage)
    {
        health.EvtDied.RemoveListener(OnUnitDied);
        spawned.Remove(health.GetComponent<Unit>());
        health.Pool();
    }

    public void StartAllUnitMovement()
    {
        for (int i = 0; i < spawned.Count; i++)
        {
            spawned[i].Movement.enabled = true;
        }
    }

    public void StopAllUnitMovement()
    {
        for (int i = 0; i < spawned.Count; i++)
        {
            spawned[i].Movement.enabled = false;
        }
    }
}
