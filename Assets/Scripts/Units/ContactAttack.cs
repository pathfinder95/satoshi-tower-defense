using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class ContactAttack : MonoBehaviour
{
    [Header("Configuration")]
    [SerializeField] private int _damage = 10;

    private Unit unit;

    private void Awake()
    {
        unit = GetComponent<Unit>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<Team>().IsEnemy(this.unit.Team)) return;

        Health health = other.GetComponent<Health>();
        if (!health) return;

        other.GetComponent<Health>().TakeDamage(_damage);
        unit.Health.Die();
    }
}
