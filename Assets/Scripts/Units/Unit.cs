using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitType
{
    Ground,
    Air
}

public class Unit : MonoBehaviour
{
    [Header("Configuration")]
    [SerializeField] private string _id;
    [SerializeField] private string _name;
    [SerializeField] private UnitType _type;

    public string Id => _id;
    public string Name => _name;
    public UnitType Type => _type;

    public Team Team { get; private set; }
    public Health Health { get; private set; }
    public Movement Movement { get; private set; }
    public CapsuleCollider Collider { get; private set; }

    private StageMode stageMode;

    public void Initialize()
    {
        if (!Team) Team = GetComponent<Team>();
        if (!Health) Health = GetComponent<Health>();
        if (!Movement) Movement = GetComponent<Movement>();
        if (!Collider) Collider = GetComponent<CapsuleCollider>();
        if (!stageMode) stageMode = GameMode.GetActive<StageMode>();

        Health.Restore();
        Movement.CurrentPathIndex = 0;
        Movement.Wave = stageMode.CurrentWave;
        Movement.enabled = true;
    }

    public Vector3 GetPosition()
    {
        return this.transform.position + Collider.center;
    }
}
