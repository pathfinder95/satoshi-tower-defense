using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Team : MonoBehaviour
{
    public static readonly string PlayerTeamId = "player";
    public static readonly string EnemyTeamId = "enemy";

    public string Id { get; set; }

    public bool IsEnemy(Team team)
    {
        return Id != team.Id;
    }
}
