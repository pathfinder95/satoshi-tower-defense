using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StageMode : GameMode
{
    public UnityEvent<int> EvtWaveStarted { get; } = new();
    public UnityEvent<int> EvtWaveEnded { get; } = new();
    public UnityEvent EvtGameStarted { get; } = new();
    public UnityEvent EvtGameLost { get; } = new();

    [Header("Configuration")]
    [SerializeField] private int _initialFunds = 100;
    [SerializeField] private CountdownTimer _fundsGainTimer = new();
    [SerializeField] private CountdownTimer _scoreGainTimer = new();
    [SerializeField] private int _fundsPerInterval = 10;
    [SerializeField] private int _scorePerInterval = 10;

    [Header("Wave")]
    [SerializeField] private CountdownTimer _waveTimer = new();
    [SerializeField] private LinearCurve _groundSpawnCurve;
    [SerializeField] private LinearCurve _airSpawnCurve;
    [SerializeField] private int _airSpawnWaveStart = 3;

    [Header("References")]
    [SerializeField] private PlayerCore _playerCore = null;
    [SerializeField] private Spawner _spawner;
    [SerializeField] private GameObject _spawnableGroundUnit;
    [SerializeField] private GameObject _spawnableAirUnit;

    public int CurrentWave { get; private set; } = 1;
    public float TimeUntilNextWave => _waveTimer.Remaining;

    public PlayerCore PlayerCore => _playerCore;

    private int groundWaveCounter = 1;
    private int airWaveCounter = 1;
    private bool waveCompleted = false;

    private Coroutine waveCycleHandler;

    protected override IEnumerator OnLoad()
    {
        _spawner.EvtSpawningCompleted.AddListener(OnSpawningCompleted);
        _spawner.EvtSpawned.AddListener(OnSpawned);

        _playerCore.PlayerState.AddFunds(_initialFunds);

        EvtGameStarted.Invoke();

        // Start Waves
        waveCycleHandler = StartCoroutine(WaveCycle());
        
        // Initialize Resource Interval
        StartResourceGain();

        // Initialize Game Conditions
        _playerCore.Health.EvtDied.AddListener(OnPlayerDied);

        yield return null;
    }

    #region Wave
    private IEnumerator WaveCycle()
    {
        _waveTimer.Start(this);

        while (true)
        {
            yield return new WaitUntil(() => _waveTimer.Ready);

            // Ground Spawn
            int groundUnitAmount = _groundSpawnCurve.GetIntValue(groundWaveCounter);
            _spawner.Spawn(_spawnableGroundUnit, groundUnitAmount);
            groundWaveCounter++;


            // Air Spawn
            if (CurrentWave >= _airSpawnWaveStart)
            {
                int airUnitAmount = _airSpawnCurve.GetIntValue(airWaveCounter);
                _spawner.Spawn(_spawnableAirUnit, airUnitAmount);
                airWaveCounter++;

                Debug.Log("Wave: " + CurrentWave + "; Ground: " + groundUnitAmount + "; Air: " + airUnitAmount);
            }
            else
            {
                Debug.Log("Wave: " + CurrentWave + "; Ground: " + groundUnitAmount);
            }

            EvtWaveStarted.Invoke(CurrentWave);

            // Wait till spawner is done
            yield return new WaitUntil(() => waveCompleted);
            yield return new WaitUntil(() => _spawner.Spawned.Count <= 0);
            EvtWaveEnded.Invoke(CurrentWave);

            CurrentWave++;
            waveCompleted = false;
            _waveTimer.Clear();
        }
    }

    private void OnSpawned(Unit unit)
    {
        //Debug.Log("Spawned: " + unit, unit.gameObject);
    }

    private void OnSpawningCompleted()
    {
        waveCompleted = true;
    }
    #endregion

    #region Resources
    public void StartResourceGain()
    {
        _fundsGainTimer.EvtReady.AddListener(OnFundsInterval);
        _scoreGainTimer.EvtReady.AddListener(OnScoreInterval);

        _fundsGainTimer.Start(this);
        _scoreGainTimer.Start(this);
    }

    private void OnFundsInterval(CountdownTimer timer)
    {
        _playerCore.PlayerState.AddFunds(_fundsPerInterval);
        _fundsGainTimer.Clear();
    }

    private void OnScoreInterval(CountdownTimer timer)
    {
        _playerCore.PlayerState.AddScore(_scorePerInterval);
        _scoreGainTimer.Clear();
    }

    public void StopResourceGain()
    {
        _fundsGainTimer.Stop();
        _scoreGainTimer.Stop();

        _fundsGainTimer.EvtReady.RemoveListener(OnFundsInterval);
        _scoreGainTimer.EvtReady.RemoveListener(OnScoreInterval);
    }
    #endregion

    #region Game Conditions
    private void OnPlayerDied(Health health, bool combatDamage)
    {
        StopResourceGain();
        _spawner.StopSpawning();
        _spawner.StopAllUnitMovement();
        SingletonManager.Get<BuildingManager>().StopAllTurretAttacks();
        Lose();
    }

    private void Lose()
    {
        PlayerData playerData = SingletonManager.Get<PlayerData>();
        playerData.HighestScore = Mathf.Max(playerData.HighestScore, PlayerCore.PlayerState.Score);

        SingletonManager.Get<CollectiveUIController>().Show("gameover");
        EvtGameLost.Invoke();
    }
    #endregion

    public void LoadMainMenu()
    {
        GameInstance.LoadGameMode("MainMenu");
    }
}
