using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [Header("Configuration")]
    [SerializeField] private string _id = null;
    [SerializeField] private GameObject _waypoint = null;

    public string Id => _id;
    public Transform Waypoint => _waypoint.transform;
}
