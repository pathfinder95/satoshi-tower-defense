using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TurretTargeting))]
public class TurretAttack : MonoBehaviour
{
    [Header("Turret")]
    [SerializeField] private float _turretRotationSpeed = 5.0f;
    [SerializeField] private float _barrelRotationSpeed = 5.0f;
    [SerializeField] private bool _clampBarrelRotation = false;
    [SerializeField] private float _minBarrelRotation = -90.0f;
    [SerializeField] private float _maxBarrelRotation = 10.0f;

    [Header("Projectile")]
    [SerializeField] private CountdownTimer _cooldown = new();
    [SerializeField] private Vector3 _projectileOffset;

    [Header("References")]
    [SerializeField] private Transform[] _projectileSpawnPoints;
    [SerializeField] private GameObject _projectile;
    [SerializeField] private GameObject _turret;
    [SerializeField] private GameObject _barrel;

    [Header("Effects")]
    [SerializeField] private GameObject _muzzleVFX;

    public Unit Target { get; private set; } = null;

    private Team team;
    private TurretTargeting turretTargeting;

    private void Awake()
    {
        team = GetComponent<Team>();
        turretTargeting = GetComponent<TurretTargeting>();
    }

    private void Start()
    {
        _cooldown.Start(this);
    }

    private void OnEnable()
    {
        if (_cooldown.Paused) _cooldown.Paused = false;
    }

    private void OnDisable()
    {
        if (!_cooldown.Paused) _cooldown.Paused = true;
    }

    private void Update()
    {
        if (!Target)
        {
            if (!turretTargeting.HasTargets) return;
            else
            {
                Target = turretTargeting.GetNearestTarget();
            }
        }

        if (!turretTargeting.TargetInRange(Target) || 
            !turretTargeting.TargetInSights(Target) || 
            !Target.Health.IsAlive)
        {
            Target = null;
            return;
        }

        if (TurretTracking() || BarrelTracking())
        {
            if (_cooldown.Ready)
            {
                Fire();
                _cooldown.Clear();
            }
        }
    }

    // Returns True if target is turret-aligned
    private bool TurretTracking()
    {   
        Vector3 lookAtTarget = new(Target.transform.position.x, _turret.transform.position.y, Target.transform.position.z);
        Vector3 direction = lookAtTarget - _turret.transform.position;

        Quaternion newRotation = Quaternion.LookRotation(direction);
        float yDelta = _turret.transform.rotation.eulerAngles.y - newRotation.eulerAngles.y;

        // If Y axis delta is greater than 0.5f Angle, rotate
        if (Mathf.Abs(yDelta) > 0.5f)
        {
            _turret.transform.rotation = Quaternion.Slerp(_turret.transform.rotation, newRotation, Time.deltaTime * _turretRotationSpeed);            
            return false;
        }

        return true;
    }

    // Returns True if target is barre-aligned
    private bool BarrelTracking()
    {
        Vector3 lookAtTarget = Target.GetPosition();
        Vector3 direction = lookAtTarget - _turret.transform.position;

        Quaternion newRotation = Quaternion.LookRotation(direction);
        float newRotX = newRotation.eulerAngles.x;

        if (_clampBarrelRotation)
        {
            float xAxis = Mathf.Clamp(newRotation.eulerAngles.x, _minBarrelRotation, _maxBarrelRotation);
            newRotation = Quaternion.Euler(xAxis, newRotation.eulerAngles.y, newRotation.eulerAngles.z);
        }

        float xDelta = _barrel.transform.rotation.eulerAngles.x - newRotX;

        if (Mathf.Abs(xDelta) > 0.5f)
        {
            _barrel.transform.rotation = Quaternion.Slerp(_barrel.transform.rotation, newRotation, Time.deltaTime * _barrelRotationSpeed);
            return false;
        }

        return true;
    }

    private void Fire()
    {
        Transform spawnPoint = _projectileSpawnPoints[Random.Range(0, _projectileSpawnPoints.Length)];

        // Muzzle VFX
        GameObject vfx = PrefabPool.Get(_muzzleVFX);
        vfx.transform.position = spawnPoint.position;
        vfx.GetComponent<ParticleTrigger>().Play(true);

        // Create Projectile
        GameObject projectile = PrefabPool.Get(_projectile);
        projectile.transform.position = spawnPoint.position;
        projectile.transform.rotation = spawnPoint.rotation;
        projectile.GetComponent<Team>().Id = this.team.Id;
    }
}
