using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;

[ExecuteInEditMode]
public class BoardCreator : MonoBehaviour
{
#if UNITY_EDITOR
    private static BoardCreator instance;

    [System.Serializable]
    public struct TileData
    {
        public char Id;
        public GameObject Prefab;
    }

    [Header("Configuration")]
    [SerializeField] private TileData[] _tileDatabase;
    [SerializeField] [TextArea] private string _boardLayout;
    [SerializeField] private float _cellSize;
    [SerializeField] private Vector3 _offset;
    [SerializeField] GameObject _startTile;
    [SerializeField] private GameObject _endTile;
    [SerializeField] private LayerMask _tileMask;

    [Header("Output")]
    [SerializeField] public Transform[] _roadWaypoints;

    private Dictionary<char, GameObject> tileDictionary = new();
    private List<GameObject> tiles = new();

    private void Awake()
    {
        tiles = GetComponentsInChildren<Tile>().Select(t => t.gameObject).ToList();
    }

    private void Update()
    {
        instance = this;
    }

    [MenuItem("Tools/Set Roads")]
    private static void SetRoads()
    {
        Vector3[] directions = { Vector3.forward, Vector3.back, Vector3.left, Vector3.right };
        List<Transform> sortedRoadTiles = new();
        List<Transform> roadTiles = instance.tiles.Where(t => t.GetComponent<Tile>().Id == "road").Select(t => t.transform).ToList();
        Transform startTile = instance._startTile.transform;
        sortedRoadTiles.Add(startTile);

        Transform currentTile = startTile;

        for (int i = 0; i < roadTiles.Count; i++)
        {
            Transform selectedTile = null;

            // Checks each face if there is another Road Tile
            for (int j = 0; j < directions.Length; j++)
            {
                if (Physics.Raycast(currentTile.position, currentTile.TransformDirection(directions[j]), out RaycastHit hit, 1.0f, instance._tileMask))
                {
                    selectedTile = hit.collider.transform;
                    if (!sortedRoadTiles.Contains(selectedTile))
                    {
                        sortedRoadTiles.Add(selectedTile);
                        currentTile = selectedTile;

                        // Stops if the End Tile is reached
                        if (currentTile == instance._endTile.transform) break;
                        continue;
                    }
                }
            }

            // Stops if the End Tile is reached
            if (currentTile == instance._endTile.transform) break;
        }

        instance._roadWaypoints = sortedRoadTiles.Select(t => t.GetComponent<Tile>().Waypoint).ToArray();
    }

    [MenuItem("Tools/Create Board")]
    private static void CreateBoard()
    {
        // Clears Tile Dictionary
        instance.tileDictionary.Clear();

        // Creates Tile Dictionary for easier look up
        for (int i = 0; i < instance._tileDatabase.Length; i++)
        {
            TileData data = instance._tileDatabase[i];
            instance.tileDictionary.Add(data.Id, data.Prefab);
        }

        // Destroys and clears all created tiles
        for (int i = 0; i < instance.tiles.Count; i++)
        {
            DestroyImmediate(instance.tiles[i]);
        }

        instance.tiles.Clear();

        // Ignores creation if layout is empty
        if (instance._boardLayout == string.Empty) return;

        // Splits to rows from new lines
        string[] boardRows = instance._boardLayout.Split('\n');

        // Column count is determined by the first row
        int columns = instance._boardLayout.Substring(0, instance._boardLayout.IndexOf('\n')).Length;
        int rows = boardRows.Length;

        for (int i = rows; i > 0; i--)
        {
            for (int j = 0; j < columns; j++)
            {
                GameObject prefab = instance.tileDictionary[boardRows[rows - i][j]];
                GameObject newTile = Instantiate(prefab, instance.transform);
                newTile.transform.localPosition = (new Vector3(j, 0.0f, i) * instance._cellSize) - (new Vector3(columns - 1, 0.0f, rows + 1) * 0.5f) + instance._offset;

                instance.tiles.Add(newTile);
            }
        }
    }
#endif
}
