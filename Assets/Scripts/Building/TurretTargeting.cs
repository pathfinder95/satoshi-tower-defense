using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(SphereCollider))]
public class TurretTargeting : MonoBehaviour
{
    [Header("Configuration")]
    [SerializeField] private float _range = 4.0f;
    [SerializeField] private Vector3 _sightOffset;
    [SerializeField] private UnitType _targetingType;
    [SerializeField] private LayerMask _targetMasking;

    [Header("References")]
    [SerializeField] private SphereCollider _sphereCollider;

    public bool Enabled { get; set; } = true;
    public List<Unit> Targets { get; private set; } = new();
    public bool HasTargets => Targets.Count > 0;

    private BaseBuilding building;

    private void Awake()
    {
        building = GetComponent<BaseBuilding>();
        _sphereCollider.radius = _range;
    }

    public Unit GetNearestTarget()
    {
        if (Targets.Count <= 0) return null;
        
        Unit unit = Targets[0];

        for (int i = 0; i < Targets.Count; i++)
        {
            if (!Targets[i]) continue;
            if (!Targets[i].Health.IsAlive) continue;

            float currentTargetDistance = Vector3.Distance(this.transform.position, unit.transform.position);
            float nextTargetDistance = Vector3.Distance(this.transform.position, Targets[i].transform.position);

            if (nextTargetDistance < currentTargetDistance)
            {
                if (TargetInSights(Targets[i]))
                {
                    unit = Targets[i];
                }
            }
        }

        return unit;

        // Linq
        //return Targets.OrderBy(t => Vector3.Distance(this.transform.position, t.transform.position)).FirstOrDefault();
    }

    public bool TargetInRange(Unit unit)
    {
        return Targets.Contains(unit);
    }

    public bool TargetInSights(Unit unit)
    {
        Vector3 direction = unit.GetPosition() - (this.transform.position + _sightOffset);

        if (Physics.Raycast((this.transform.position + _sightOffset), direction, out RaycastHit hit, _range, _targetMasking))
        {
            Debug.DrawRay((this.transform.position + _sightOffset), direction, Color.cyan);

            if (hit.collider.GetComponent<Unit>())
            {
                return true;
            }
        }

        return false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<Team>().IsEnemy(this.building.Team)) return;

        Unit unit = other.GetComponent<Unit>();

        if (!unit.Health.IsAlive) return;
        if (unit.Type != _targetingType) return;
        if (Targets.Contains(unit)) return;

        Targets.Add(unit);
        unit.Health.EvtDied.AddListener(OnTargetDied);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.GetComponent<Team>().IsEnemy(this.building.Team)) return;

        Unit unit = other.GetComponent<Unit>();

        if (Targets.Contains(unit))
        {
            Targets.Remove(unit);
            unit.Health.EvtDied.RemoveListener(OnTargetDied);
        }

        for (int i = Targets.Count - 1; i > -1; i--)
        {
            if (Targets[i] == null)
            {
                Targets.RemoveAt(i);
                continue;
            }

            if (!Targets[i].Health.IsAlive) Targets.RemoveAt(i);
        }

        // Linq
        // Targets = Targets.Where(t => t).ToList();
    }

    private void OnTargetDied(Health health, bool combatDamage)
    {
        Unit unit = health.GetComponent<Unit>();
        unit.Health.EvtDied.RemoveListener(OnTargetDied);
        Targets.Remove(unit);
    }
}
