using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;

public class BuildingManager : MonoBehaviour
{
    public UnityEvent<BaseBuildingPivot> EvtShow { get; } = new();
    public UnityEvent EvtHide { get; } = new();

    [Header("Configuration")]
    [SerializeField] private float _refundModifier = 0.5f;
    [SerializeField] private string _teamId = null;

    [Header("References")]
    [SerializeField] private GameObject[] _pivots;
    [SerializeField] private GameObject[] _turrets;

    public GameObject[] BuildableTurrets => _turrets;

    private StageMode stageMode;

    private void Awake()
    {
        SingletonManager.Register(this);
    }

    private void Start()
    {
        stageMode = GameMode.GetActive<StageMode>();
    }

    public void ShowBuildingPanel(BaseBuildingPivot pivot)
    {
        EvtShow.Invoke(pivot);
    }

    public void HideBuildingPanel()
    {
        EvtHide.Invoke();
    }

    public bool CanPurchase(BaseBuilding building)
    {
        return building.BuildCost <= stageMode.PlayerCore.PlayerState.Funds;
    }

    public void Purchase(BaseBuildingPivot pivot, BaseBuilding building)
    {
        Assert.IsTrue(CanPurchase(building), "Not enough Funds to purchase: " + building.Id);

        // Charges funds for building cost
        stageMode.PlayerCore.PlayerState.TryTakeFunds(building.BuildCost);
        Build(pivot, building);
    }

    private void Build(BaseBuildingPivot pivot, BaseBuilding building)
    {
        BaseBuilding newBuilding = Instantiate(building);
        newBuilding.Team.Id = _teamId;
        pivot.SetBuilding(newBuilding);
    }

    public void Sell(BaseBuildingPivot pivot)
    {
        Assert.IsTrue(pivot.CurrentBuilding, "No building currently placed at pivot");

        // Refunds half of building cost
        stageMode.PlayerCore.PlayerState.AddFunds(Mathf.RoundToInt(pivot.CurrentBuilding.BuildCost * _refundModifier));
        Demolish(pivot);
    }

    private void Demolish(BaseBuildingPivot pivot)
    {
        pivot.RemoveBuilding();
    }

    public void StartAllTurretAttacks()
    {
        for (int i = 0; i < _pivots.Length; i++)
        {
            BaseBuildingPivot pivot = _pivots[i].GetComponent<BaseBuildingPivot>();
            if (pivot.CurrentBuilding)
            {
                pivot.CurrentBuilding.GetComponent<TurretAttack>().enabled = true;
            }
        }
    }

    public void StopAllTurretAttacks()
    {
        for (int i = 0; i < _pivots.Length; i++)
        {
            BaseBuildingPivot pivot = _pivots[i].GetComponent<BaseBuildingPivot>();
            if (pivot.CurrentBuilding)
            {
                pivot.CurrentBuilding.GetComponent<TurretAttack>().enabled = false;
            }
        }
    }
}
