﻿using UnityEngine;
using UnityEngine.Events;

public class BaseBuilding : MonoBehaviour
{
    [Header("Configuration")]
    [SerializeField] private string _id = null;
    [SerializeField] private string _name = null;
    [SerializeField] private Sprite _uiIcon = null;
    [SerializeField] private int _buildCost = 40;

    public string Id => _id;
    public string Name => _name;
    public Sprite UIIcon => _uiIcon;
    public int BuildCost => _buildCost;

    public bool IsPlaced { get; protected set; }
    public Team Team { get; private set; }

    private void Awake()
    {
        Team = GetComponent<Team>();
    }

    public void SetParentPivot(Transform pivotTransform)
    {
        transform.SetParent(pivotTransform);
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
    }

    public virtual void OnPlacement() { IsPlaced = true; }

    public virtual void OnRemoval() { Destroy(gameObject); }
}
