using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public UnityEvent<int> EvtTakenDamage { get; } = new();

    // bool, if died in Combat
    public UnityEvent<Health, bool> EvtDied { get; } = new();

    [Header("Configuration")]
    [SerializeField] private LinearCurve _hpCurve;

    public int Wave { get; set; } = 1;
    public int CurrentHp { get; private set; }
    public int MaxHp => _hpCurve.GetIntValue(Wave);
    public bool IsAlive => CurrentHp > 0;
    public float Normalized => CurrentHp / (float)MaxHp;

    public void TakeDamage(int amount, bool combatDamage = true)
    {
        if (!IsAlive) return;

        CurrentHp = Mathf.Max(0, CurrentHp - amount);
        EvtTakenDamage.Invoke(amount);

        if (!IsAlive) EvtDied.Invoke(this, combatDamage);
    }

    public void Restore()
    {
        CurrentHp = MaxHp;
    }

    public void Die()
    {
        TakeDamage(CurrentHp, false);
    }
}
