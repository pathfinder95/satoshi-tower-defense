using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class LinearProjectile : MonoBehaviour, IPoolable
{
    public UnityEvent EvtHit { get; } = new();

    [Header("Configuration")]
    [SerializeField] private string _id = null;
    [SerializeField] private string _name = null;
    [SerializeField] private int _damage = 10;
    [SerializeField] private float _speed = 10.0f;
    [SerializeField] private float _range = 10.0f;

    public string Id => _id;
    public string Name => _name;
    public Vector3 StartPosition { get; protected set; }
    public float Distance => Mathf.Abs(transform.position.x - StartPosition.x);
    public Team Team { get; private set; }

    protected Rigidbody rb;
    private bool destroying = false;
    private bool hasStarted = false;

    private void InitializeProjectile()
    {
        Team = GetComponent<Team>();
        rb = GetComponent<Rigidbody>();

        StartPosition = transform.position;
    }

    private void OnHit(Unit unit)
    {
        unit.Health.TakeDamage(_damage);
        EvtHit.Invoke();
    }

    private void DestroyProjectile()
    {
        destroying = true;
        this.Pool();
    }

    private void FixedUpdate()
    {
        if (destroying) return;

        // First frame is for intializing
        if (!hasStarted)
        {
            hasStarted = true;
            InitializeProjectile();
            return;
        }

        // Destroy when projectile is out of range
        if (Distance > _range && _range > 0)
        {
            Debug.Log("Out of Range!", this.gameObject);
            DestroyProjectile();
        }

        Vector3 newPosition = rb.position + (_speed * Time.deltaTime * transform.forward);
        rb.MovePosition(newPosition);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (destroying) return;
        if (!other.attachedRigidbody) return;
        if (!other.GetComponent<Team>().IsEnemy(this.Team)) return;

        OnHit(other.GetComponent<Unit>());
        DestroyProjectile();
    }

    void IPoolable.OnPoolAwake()
    {

    }

    void IPoolable.OnPoolSleep()
    {
        destroying = false;
        hasStarted = false;

        StartPosition = Vector3.zero;
    }
}